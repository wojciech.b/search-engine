package com.searchengine

import java.io.File

import com.searchengine.ranking.{BasicRank, ComplexRank}

object SearchEngine {

  def main(args: Array[String]): Unit = {
    args.length match {
      case 0 => println("No directory given to index.")
      case _ =>
        val files = getFilesFromDirectory(args(0))
        if(files.nonEmpty) {
          println(s"${files.length} files read from directory ${args(0)}")

          val wordCountMapsList = Parser.getFilenameWordMapPair(files)

          val scanner = new java.util.Scanner(System.in)


          println("To exit type :quit")
          println("Do you want to use complex ranking? (y/n)")
          val rank = scanner.nextLine match {
            case "2" =>
              println("Using complex ranking")
              new ComplexRank
            case _ =>
              println("Using default basic ranking")
              new BasicRank
          }

          while (true) {
            print("search> ")
            val inputWord = scanner.nextLine
            inputWord match {
              case ":quit" => System.exit(0)
              case _ if inputWord.length > 0=>
                val inputWordsList = Parser.splitText(inputWord).distinct
                val scoreForFile = wordCountMapsList.map {
                  map =>
                    val score = rank.getScore(inputWordsList, map._2)
                    (map._1, score)
                }
                //Sorting pairs and printing result- highest scores first
                scoreForFile.sortWith(_._2 > _._2).take(10).foreach {
                  case (filename, score) => println(s"$filename: $score%")
                }
              case _ => //do nothing

            }

          }
        }
    }
  }
  def getFilesFromDirectory(directory: String): Seq[File] = {
    val dir = new File(directory)
    if(!dir.exists || !dir.isDirectory) {
      println("Directory not exists")
      Seq.empty
    } else
      dir.listFiles
  }
}
