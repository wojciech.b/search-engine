package com.searchengine

import java.io.File

import scala.io.Source

object Parser {
  //Iterate over sequence of files and create sequence of pairs of filename and word counts map
  def getFilenameWordMapPair(files: Seq[File]): Seq[(String,Map[String,Int])] = {
    files.map {
      file =>
        val fileContentlist = Source.fromFile(file).getLines().flatMap(line => splitText(line)).toList //.getLines.mkstring concatenates last and first word from next line
        (file.getName, getWordsCountMap(fileContentlist)) // (filename, mapOfWords) pair
    }
  }
  //get map from text where keys are words and values are number of their occurrence in given text
  def getWordsCountMap(text: List[String]): Map[String, Int] = {
    text.groupBy(identity).mapValues(_.length)
  }
  def splitText(text: String) = text.toLowerCase.split(splitRegex).toList

  val splitRegex = "([:,.?!\"]|\\s)+"

}
