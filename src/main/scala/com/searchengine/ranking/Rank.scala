package com.searchengine.ranking

trait Rank {

  def getScore(searchWords: Seq[String], wordCountMap: Map[String,Int]): Int

}
