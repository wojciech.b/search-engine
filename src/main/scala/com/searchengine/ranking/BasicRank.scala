package com.searchengine.ranking

class BasicRank extends Rank{
  override def getScore(searchWords: Seq[String], wordCountMap: Map[String, Int]): Int = {
    val wordsFound = searchWords.foldLeft(0){
      (b,a) =>
        val toAdd = wordCountMap.keySet.contains(a) match {
          case true => 1
          case _ => 0
        }
        toAdd + b
    }

    Math.round(wordsFound.toFloat / searchWords.length * 100)

  }
}
