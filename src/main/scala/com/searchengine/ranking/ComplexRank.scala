package com.searchengine.ranking

class ComplexRank extends Rank{
  override def getScore(searchWords: Seq[String], wordCountMap: Map[String, Int]): Int = {
    val wordsFound = searchWords.foldLeft(0){
      (sum, key) =>
        val toAdd = wordCountMap.keySet.contains(key) match {
          case true => 1
          case _ => 0
        }
        toAdd + sum
    }
    val fileNumberOfWords = wordCountMap.foldLeft(0){
      (sum, key) =>
        val toAdd = wordCountMap.keySet.contains(key._1) match {
          case true => wordCountMap(key._1)
          case _ => 0
        }
        toAdd + sum
    }
    val searchWordsNumberInFile = searchWords.foldLeft(0){
      (sum, key) =>
        val toAdd = wordCountMap.keySet.contains(key) match {
          case true => wordCountMap(key)
          case _ => 0
        }
        toAdd + sum
    }
    //number of words found
    val percentPerWord = ((searchWordsNumberInFile.toFloat / fileNumberOfWords) / searchWords.size) * 100
    val result = (wordsFound.toFloat / searchWords.length) * 100
    if(result <100 && result > 0)
      Math.round(result + percentPerWord)
    else
      Math.round(result)
  }
}
