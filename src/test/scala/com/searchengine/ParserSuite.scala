package com.searchengine

import org.specs2.mutable._

class ParserSuite extends Specification {

  val simpleText = "Simple text to split"
  val complexText = "Complex text, there are punctuation marks. Test!"
  val occurencesText = "One two two three three three"
  val bigSmalLettersText = "BIG small mIXeD"
  "Parser" should {
    "Split simple text" in {
      Parser.splitText(simpleText).length === 4
    }
    "Split complex text" in {
      val wordList = Parser.splitText(complexText)
      wordList.contains("text") === true
      wordList.contains("text,") === false
    }
    "Create map of words occurrences" in {
      val wordCountMap = Parser.getWordsCountMap(Parser.splitText(occurencesText))
      wordCountMap("one") === 1
      wordCountMap("two") === 2
      wordCountMap("three") === 3
    }
    "Create map with only lowercase words" in {
      val wordCountMap = Parser.getWordsCountMap(Parser.splitText(bigSmalLettersText))
      wordCountMap.contains("BIG") === false
      wordCountMap.contains("big") === true
      wordCountMap.contains("small") === true
      wordCountMap.contains("mIXeD") === false
    }
  }

}
