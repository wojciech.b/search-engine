package com.searchengine

import com.searchengine.ranking.{BasicRank, ComplexRank}
import org.specs2.mutable._

class RankingSuite extends Specification {

  private val testContentList = Parser.splitText("Sample test content for rating")
  private val wordCountMap = Parser.getWordsCountMap(testContentList)
  "Basic ranking" should {
    val ranking = new BasicRank
    "be 100%" in {
      val testInput = Parser.splitText("content rating")
      ranking.getScore(testInput, wordCountMap) === 100
    }
    "be 0%" in {
      val testInput = Parser.splitText("word not found")
      ranking.getScore(testInput, wordCountMap) === 0
    }
    "be 50%" in {
      val testInput = Parser.splitText("sample not ")
      ranking.getScore(testInput, wordCountMap) === 50
    }
    "be less than 50%" in {
      val testInput = Parser.splitText("sample not found")
      ranking.getScore(testInput, wordCountMap) must beLessThan(50)
    }
  }

  "Complex ranking" should {
    val ranking = new ComplexRank
    "be 100%" in {
      val testInput = Parser.splitText("content rating")
      ranking.getScore(testInput, wordCountMap) === 100
    }
    "be 0%" in {
      val testInput = Parser.splitText("word not found")
      ranking.getScore(testInput, wordCountMap) === 0
    }
  }

}
