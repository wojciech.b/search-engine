# Search engine
Command line driven search engine

# Build
Move to the project directory and run
```
sbt assembly
```
This will create SearchEngine.jar in target/scala-2.12 folder.
# Running application
```
java -jar SearchEngine.jar directoryContainingTextFiles
```
# Running tests
```
sbt test
```

# Rating algorithms
* basic - checks if file contains all given words
* complex - checks if file contains all given words and if not ranks result by the percentage of words found and number of words in file

# Author
Wojciech Bator