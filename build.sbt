name := "SearchEngine"

version := "1.0"

scalaVersion := "2.12.6"

libraryDependencies += "org.specs2" %% "specs2-core" % "4.3.0" % Test

mainClass in (Compile, run) := Some("com.searchengine.SearchEngine")

assemblyJarName in assembly := "SearchEngine.jar"